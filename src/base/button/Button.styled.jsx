import styled from 'styled-components';
import { globalStyles } from '../../global';
import PropTypes from 'prop-types';

export const Button = styled.button`
  border: none;
  color: white;
  text-transform: uppercase;
  cursor: pointer;
  font-family: 'Roboto', 'Helvetica', 'Arial', sans-serif;
  background-color: ${globalStyles.background.primary};

  &:disabled {
    background-color: ${globalStyles.background.disabled};
    color: ${globalStyles.color.disabled};
  }
`;

Button.propTypes = {
  large: PropTypes.bool,
  medium: PropTypes.bool,
};
