import React from 'react';
import * as Styles from './Button.styled';

export const Button = ({ children, ...props }) => {
  return <Styles.Button {...props}>{children}</Styles.Button>;
};
