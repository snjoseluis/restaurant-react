export const responseCodes = {
  USER_CREATED: 'USER_CREATED',
  USER_FOUND: 'USER_FOUND',
  INCORRECT_PASSWORD: 'INCORRECT_PASSWORD',
  USER_LOGIN: 'USER_LOGIN',
};
