export const allElements = {
  menuElements: [
    {
      name: 'Pizzas',
      link: '/menu/pizzas',
      id: '_pizzas',
    },
    {
      name: "P'Zone",
      link: '/menu/p-zone',
      id: '_pZones',
    },
    {
      name: 'Wings',
      link: '/menu/wings',
      id: '_wings',
    },
    {
      name: 'Sides',
      link: '/menu/sides',
      id: '_sides',
    },
    {
      name: 'Pasta',
      link: '/menu/pasta',
      id: '_pasta',
    },
    {
      name: 'Desserts',
      link: '/menu/desserts',
      id: '_desserts',
    },
    {
      name: 'Drinks',
      link: '/menu/drinks',
      id: '_drinks',
    },
  ],
  ourFoodEle: [
    {
      id: 10,
      name: 'Restaurant Quality Food',
      link: '/',
    },
    {
      id: 11,
      name: 'Nutrition',
      link: '/',
    },
    {
      id: 12,
      name: 'Gluten Free',
      link: '/',
    },
  ],
  aboutUs: [
    {
      id: 13,
      name: 'Our Story',
      link: '/',
    },
    {
      id: 14,
      name: 'Hut Life Blog',
      link: '/',
    },
    {
      id: 15,
      name: 'Careers',
      link: '/',
    },
    {
      id: 16,
      name: 'Catering',
      link: '/',
    },
    {
      id: 17,
      name: 'International',
      link: '/',
    },
    {
      id: 18,
      name: 'Become A Franchisee',
      link: '/',
    },
    {
      id: 19,
      name: 'Community Affairs Support',
      link: '/',
    },
    {
      id: 20,
      name: 'Supplier Code',
      link: '/',
    },
    {
      id: 21,
      name: 'Diversity',
      link: '/',
    },
    {
      id: 22,
      name: 'School Lunch',
      link: '/',
    },
    {
      id: 23,
      name: 'BOOK IT! program',
      link: '/',
    },
  ],
  costumerService: [
    {
      id: 24,
      name: 'COVID-19 Updates',
      link: '/',
    },
    {
      id: 25,
      name: 'Contact US',
      link: '/',
    },
    {
      id: 26,
      name: 'Find a Pizza Hut',
      link: '/',
    },
    {
      id: 27,
      name: 'Restaurants By State',
      link: '/',
    },
    {
      id: 28,
      name: 'FAQs',
      link: '/',
    },
    {
      id: 29,
      name: 'Gift Cards',
      link: '/',
    },
    {
      id: 30,
      name: 'Food Delivery',
      link: '/',
    },
    {
      id: 31,
      name: 'Restaurants Open',
      link: '/',
    },
    {
      id: 32,
      name: 'Español',
      link: '/',
    },
  ],
  myAccount: [
    {
      id: 33,
      name: 'Create an Account',
      link: '/',
    },
    {
      id: 34,
      name: 'Sign In',
      link: '/',
    },
  ],
};

export const pizzas = {
  popularPizzas: [
    {
      id: 1,
      name: 'Cheese Pizza',
      description:
        'Your favorite cheese pizza made with classic marinara sauce topped with mozzarella cheese. Order pizza online now.',
      image: '/food-menu/pizzas/popular-pizzas/pizza_Cheese.jpg',
      textButton: 'Order Now',
    },
    {
      id: 2,
      name: 'Pepperoni Pizza',
      description:
        'This gourmet pizza is ideal for easy late night delivery, complete with mozzarella cheese and pepperoni. Order pizza online now.',
      image: '/food-menu/pizzas/popular-pizzas/pizza_Pepperoni.jpg',
      textButton: 'Order Now',
    },
    {
      id: 3,
      name: "Meat Lover's® Pizza",
      description:
        "Meat lover's pizza is the perfect game day pizza, packed with pepperoni, Italian sausage, ham, bacon, seasoned pork and beef.",
      image: '/food-menu/pizzas/popular-pizzas/pizza_Meat_Lovers.jpg',
      textButton: 'Order Now',
    },
    {
      id: 4,
      name: 'Supreme Pizza',
      description:
        'Enjoy this freshly prepared pizza for family dinner or a group lunch, including pepperoni, seasoned pork, beef, mushrooms, green.',
      image: '/food-menu/pizzas/popular-pizzas/pizza_Supreme.jpg',
      textButton: 'Order Now',
    },
  ],
  meats: [
    {
      id: 1,
      name: 'Supreme Pizza',
      description:
        'Enjoy this freshly prepared pizza for family dinner or a group lunch, including pepperoni, seasoned pork, beef, mushrooms, green bell peppers and red onions. Order pizza online now.',
      image: '/food-menu/pizzas/meats/pizza_Supreme.jpg',
      textButton: 'Order Now',
    },
    {
      id: 2,
      name: "Meat Lover's Pizza",
      description:
        "Meat lover's pizza is the perfect game day pizza, packed with pepperoni, Italian sausage, ham, bacon, seasoned pork and beef. Order pizza online now.",
      image: '/food-menu/pizzas/meats/pizza_Meat_Lovers.jpg',
      textButton: 'Order Now',
    },
    {
      id: 3,
      name: "Pepperoni Lover's Pizza",
      description:
        "If you've ever wished your pepperoni pizza had more pepperoni on it, then this pizza is for you! This oven-fresh pizza has 50% more pepperoni than our average pizza pie. Order pizza online now.",
      image: '/food-menu/pizzas/meats/pizza_Pepperoni_Lovers.jpg',
      textButton: 'Order Now',
    },
  ],
  chicken: [
    {
      id: 1,
      name: 'Hawaiian Chicken Pizza',
      description:
        "What's better than your standard Hawaiian pizza? How about a Hawaiian pizza with tasty chicken, ham, pineapple and flavorful green bell peppers on it as well. Order pizza online now.",
      image: '/food-menu/pizzas/chicken/pizza_Hawaiian_Chicken.jpg',
      textButton: 'Order Now',
    },
    {
      id: 2,
      name: 'Chicken-Bacon Parmesan Pizza',
      description:
        'There are few combos better than chicken and bacon. This oven-hot pizza is also topped with Roma tomatoes and finished off with a Parmesan crust—perfect for late night take out. Order pizza online now.',
      image: '/food-menu/pizzas/chicken/pizza_Chicken_Bacon_Parmesan.jpg',
      textButton: 'Order Now',
    },
    {
      id: 3,
      name: 'Backyard BBQ Chicken Pizza',
      description:
        'A BBQ pizza topped with grilled chicken, bacon and red onion? Sign us up! This oven-hot pizza is also perfect for a group lunch or home delivery this Friday night. Order pizza online now.',
      image: '/food-menu/pizzas/chicken/pizza_Backyard_BBQ_Chicken.jpg',
      textButton: 'Order Now',
    },
    {
      id: 4,
      name: 'Buffalo Chicken Pizza',
      description:
        'This classic pizza featuring tasty buffalo sauce, tender chicken and flavorful banana peppers is your go-to for weeknight pizza delivery. Order pizza online now.',
      image: '/food-menu/pizzas/chicken/pizza_Buffalo_Chicken.jpg',
      textButton: 'Order Now',
    },
  ],
  veggie: [
    {
      id: 1,
      name: "Veggie Lover's Pizza",
      description:
        "Featuring all the classic vegetable toppings you know and love—like mushrooms, red onions, green bell peppers, Roma tomatoes and black olives—this veggie pizza a garden lover's dream! Order pizza online now.",
      image: '/food-menu/pizzas/veggie/pizza_Veggie_Lovers.jpg',
      textButton: 'Order Now',
    },
    {
      id: 2,
      name: "Ultimate Cheese Lover's Pizza",
      description:
        'With 50% more mozzarella cheese, Garlic Parmesan sauce, and a toasted Parmesan crust, this is surely the cheesiest of our cheese pizzas. Order pizza online now.',
      image: '/food-menu/pizzas/veggie/pizza_Ultimate_Cheese_Lovers.jpg',
      textButton: 'Order Now',
    },
  ],
};

export const wings = {
  wingstreetWings: [
    {
      id: 1,
      name: 'Large Traditional Wings',
      description:
        'An order of our classic, crispy bone-in wings is sure to hit the spot. All wings come with your choice of sauce – Teriyaki, Garlic Parmesan, Honey BBQ, or our new Nashville Hot!',
      image: '/food-menu/wings/wingstreet-wings/wings_6BIWings.jpg',
      textButton: 'Order Now',
    },
    {
      id: 2,
      name: 'Breaded Bone-Out Wings',
      description:
        'Like your wings bone-out? Try these tasty, 100% all-white meat chicken wings covered in savory breading and your choice of sauce, including our new Nashville Hot or Smoky Sriracha! Order wings online now.',
      image: '/food-menu/wings/wingstreet-wings/wings_8BOWings.jpg',
      textButton: 'Order Now',
    },
  ],
  wingstreetSides: [
    {
      id: 3,
      name: 'Fries',
      description:
        'Crispy fries always make a welcome addition to any of our wing meals. Choose to have them topped with our delicious seasoning and complete your online delivery today!',
      image: '/food-menu/wings/wingstreet-sides/sides_Fries.jpg',
      textButton: 'Order Now',
    },
  ],
  dips: [
    {
      id: 4,
      name: 'Blue Cheese',
      description:
        'A savory, creamy blue cheese dip will be a hit for game day delivery or any other occasion requiring delicious wings.',
      image: '/food-menu/wings/dips/sauces_BlueCheese.jpg',
      textButton: 'Order Now',
    },
    {
      id: 5,
      name: 'Garlic',
      description:
        'Garlic? Buttery parmesan? Sign us up for this flavorful wing dip. (Hint: equally delicious on pizza crust.)',
      image: '/food-menu/wings/dips/sauces_Garlic.jpg',
      textButton: 'Order Now',
    },
    {
      id: 6,
      name: 'Honey BBQ',
      description:
        'Looking for something a little sweeter on your wings or pizza? Our honey BBQ sauce is made with real honey and molasses, offering that perfect sweet and smoky flavor you crave.',
      image: '/food-menu/wings/dips/sauces_HoneyBBQ.jpg',
      textButton: 'Order Now',
    },
  ],
};

export const drinks = [
  {
    id: 1,
    name: 'PEPSI-COLA® 4-Pack',
    description: 'Four Individual Sized Beverages',
    image: '/food-menu/drinks/B1_deals_5Pepsi4Pack.jpg',
    textButton: 'Order Now',
  },
  {
    id: 2,
    name: 'PEPSI®',
    description: 'The bold, refreshing, robust cola.',
    image: '/food-menu/drinks/drinks_Pepsi_logo.png',
    textButton: 'Order Now',
  },
  {
    id: 3,
    name: 'DIET PEPSI®',
    description:
      'Light. Crisp. Refreshing. With zero sugar, zero calories, and zero carbs.',
    image: '/food-menu/drinks/drinks_dietpepsi.jpg',
    textButton: 'Order Now',
  },
  {
    id: 4,
    name: 'MTN DEW®',
    description:
      'Mountain Dew, the original instigator, refreshes with its one of a kind great taste.',
    image: '/food-menu/drinks/drinks_MtnDew_logo.png',
    textButton: 'Order Now',
  },
  {
    id: 5,
    name: 'SIERRA MIST®',
    description:
      'A crisp, refreshing & caffeine free Lemon-Lime flavor soda with real sugar and a splash of real juice.',
    image: '/food-menu/drinks/Drinks_SierraMist.png',
    textButton: 'Order Now',
  },
];

export const desserts = [
  {
    id: 1,
    name: 'Cinnabon Mini Rolls',
    description:
      '10 mini cinnamon rolls, topped with signature cream cheese frosting, are the perfect way to end pizza night.',
    image: '/food-menu/desserts/Desserts_NEWCinnabonMiniRolls.jpg',
    textButton: 'Order Now',
  },
  {
    id: 2,
    name: "Ultimate HERSHEY'S Chocolate Chip Cookie",
    description:
      "A freshly-baked cookie made with 100% genuine Hershey's Chocolate Chips will be a new favorite at family dinner",
    image: '/food-menu/desserts/desserts_cookie_175x203.jpg',
    textButton: 'Order Now',
  },
  {
    id: 3,
    name: "HERSHEY'S Triple Chocolate Brownie",
    description:
      "Calling all chocolate lovers! This warm, triple chocolate brownie features Hershey's Cocoa, Special Dark",
    image: '/food-menu/desserts/desserts_brownie_175x203.jpg',
    textButton: 'Order Now',
  },
  {
    id: 4,
    name: 'Cinnamon Sticks',
    description:
      'These freshly-baked cinnamon sticks add a sweet and crispy finale to any of our tasty pizzas. Dip into the white icing for added',
    image: '/food-menu/desserts/desserts_CinnamonSticks_Icing.jpg',
    textButton: 'Order Now',
  },
];

export const pasta = [
  {
    id: 1,
    name: 'Tuscani Chicken Alfredo Pasta',
    description:
      'Grilled chicken breast? Check. Creamy alfredo sauce? Check. Layer of cheese plus a side of breadsticks? Check and double check',
    image: '/food-menu/pasta/pasta_PanChickenAlfredo_Breadsticks.jpg',
    textButton: 'Order Now',
  },
  {
    id: 2,
    name: 'Tuscani Meaty Marinara Pasta',
    description:
      'Nothing is better than a classic Italian-seasoned meat sauce pasta topped with cheese. Accompanied by an order of breadsticks',
    image: '/food-menu/pasta/pasta_PanMeatyMarinara_Breadsticks.jpg',
    textButton: 'Order Now',
  },
];

export const pzone = [
  {
    id: 1,
    name: 'Pepperoni P’ZONE',
    description:
      'Pepperoni and melted cheese sealed inside a folded, pizza crust. Finished with toasted parmesan and baked calzone style. Served',
    image: '/food-menu/pzone/pzone_pepperoni_producttile_175x203.jpg',
    textButton: 'Order Now',
  },
  {
    id: 2,
    name: 'Supremo P’ZONE',
    description:
      'Italian Sausage, Green Pepper, Red Onion and melted cheese sealed inside a folded, pizza crust. Finished with toasted parmesan',
    image: '/food-menu/pzone/pzone_supremo_producttile_175x203.jpg',
    textButton: 'Order Now',
  },
  {
    id: 3,
    name: 'Meaty P’ZONE',
    description:
      'Pepperoni, Ham, Beef, Pork, Italian Sausage and melted cheese sealed inside a folded, pizza crust. Finished with toasted parmesan',
    image: '/food-menu/pzone/pzone_meaty_producttile_175x203.jpg',
    textButton: 'Order Now',
  },
];

export const deals = [
  {
    id: 1,
    name: 'Big Dinner Box',
    description:
      'Start with 2 medium 1-topping pizzas and 5 breadsticks plus your choice of wings or pasta. Or, choose 3 medium 1-topping pizzas.',
    image: '/food-menu/deals/Deals_BDB_WSBOWings.jpg',
    textButton: 'Order Now',
  },
  {
    id: 2,
    name: '$9.99 Large Pizza – Up To 3 Toppings, Delivery Or Carryout',
    description:
      'Choose your favorite 3 toppings on a large Pizza Hut pizza and get it any way you want it: delivery, carryout, online, or over the phone. Additional charges apply for extra toppings, extra cheese, Pan and Stuffed crust.',
    image: '/food-menu/deals/deals_L3TPizza_175x203.jpg',
    textButton: 'Order Now',
  },
  {
    id: 3,
    name: 'Big Dipper Pizza',
    description:
      'Nearly 2 feet of dippable pizza on pan-style crust. The Big Dipper™ Pizza comes without sauce under the toppings, but is served with 4 dipping sauce cups (2 marinara, 1 Ranch and 1 Honey BBQ) so you can experience different flavors.',
    image: '/food-menu/deals/pizza_Meat_Lovers.jpg',
    textButton: 'Order Now',
  },
  {
    id: 4,
    name: 'Dinner Box',
    description:
      "Don’t know what to get for take out tonight? We've made it easy! Select which freshly prepared medium 1-topping pizza you want, and you'll also get five breadsticks with marinara dipping sauce, plus 10 cinnamon sticks with icing—all in one convenient box.",
    image: '/food-menu/deals/A24_deals_10DinnerBox.jpg',
    textButton: 'Order Now',
  },
  {
    id: 5,
    name: 'Cinnabon® Mini Rolls',
    description:
      '10 mini cinnamon rolls, topped with signature cream cheese frosting, are the perfect way to end pizza night.',
    image: '/food-menu/deals/Desserts_NEWCinnabonMiniRolls.jpg',
    textButton: 'Order Now',
  },
  {
    id: 6,
    name: 'First Book Bundle',
    description:
      'Pizza Hut is on a mission to start a new chapter for young readers across America. Order the First Book Bundle that includes 2 large 3-topping pizzas and an order of breadsticks. $1 from every purchase will be donated to First Book and go towards providing access to books and educational resources.',
    image:
      '/food-menu/deals/Deals_Literacy_2L3TPizzas_Breadsticks_Donation.jpg',
    textButton: 'Order Now',
  },
  {
    id: 7,
    name: 'PEPSI-COLA 4-Pack',
    description: 'Four Individual Sized Beverages',
    image: '/food-menu/deals/B1_deals_5Pepsi4Pack.jpg',
    textButton: 'Order Now',
  },
  {
    id: 8,
    name: "$5'N Up Lineup 2 Or More, Starting At $5 Each",
    description:
      'Get Medium 1-topping pizzas, Bone-out Wings, Cinnabon® Mini Rolls and more when you try two or  Some items are more than $5.',
    image: '/food-menu/deals/deals_5NUpLUNOCHEEZ_175x203.jpg',
    textButton: 'Order Now',
  },
];

export const sides = {
  sides: [
    {
      id: 1,
      name: 'Stuffed Garlic Knots',
      description:
        'Stuffed with melted cheese, then finished with a buttery garlic blend and grated parmesan, our famous garlic knots are always a welcome addition to any pizza delivery order. Comes with a cup of our delicious marinara sauce!',
      image: '/food-menu/sides/sides/sides_StuffedGarlicKnots_Marinara.jpg',
      textButton: 'Order Now',
    },
    {
      id: 2,
      name: 'Breadsticks',
      description:
        'Nobody does classic breadsticks like Pizza Hut. Add to your pasta or pizza order for tasty crispy-on-the-outside, soft-on-the-inside bread sticks seasoned with garlic and parmesan. Comes with a cup of our delicious marinara sauce!',
      image: '/food-menu/sides/sides/sides_Breadsticks_Marinara.jpg',
      textButton: 'Order Now',
    },
    {
      id: 3,
      name: 'Cheese Sticks',
      description:
        'A side everyone can agree on for late night delivery—cheese sticks! They come sprinkled with flavorful Italian seasoning and a cup of our delicious marinara sauce.',
      image: '/food-menu/sides/sides/sides_CheeseSticks_Marinara.jpg',
      textButton: 'Order Now',
    },
    {
      id: 4,
      name: 'Pepperoni P’ZONE',
      description:
        'Pepperoni and melted cheese sealed inside a folded, pizza crust. Finished with toasted parmesan and baked calzone style. Served with marinara dipping sauce.',
      image: '/food-menu/sides/sides/pzone_pepperoni_producttile_175x203.jpg',
      textButton: 'Order Now',
    },
    {
      id: 5,
      name: 'Supremo P’ZONE',
      description:
        'Italian Sausage, Green Pepper, Red Onion and melted cheese sealed inside a folded, pizza crust. Finished with toasted parmesan and baked calzone style. Served with marinara dipping sauce.',
      image: '/food-menu/sides/sides/pzone_supremo_producttile_175x203.jpg',
      textButton: 'Order Now',
    },
    {
      id: 6,
      name: 'Meaty P’ZONE',
      description:
        'Pepperoni, Ham, Beef, Pork, Italian Sausage and melted cheese sealed inside a folded, pizza crust. Finished with toasted parmesan and baked calzone style. Served with marinara dipping sauce.',
      image: '/food-menu/sides/sides/pzone_meaty_producttile_175x203.jpg',
      textButton: 'Order Now',
    },
    {
      id: 7,
      name: 'Fries',
      description:
        'Crispy fries always make a welcome addition to any of our wing meals. Choose to have them topped with our delicious seasoning and complete your online delivery today!',
      image: '/food-menu/sides/sides/sides_Fries.jpg',
      textButton: 'Order Now',
    },
  ],
  sauces: [
    {
      id: 8,
      name: 'Marinara',
      description:
        "Our freshly prepared, signature Italian-seasoned sauce isn't just great on pizza, it's also great for wings!",
      image: '/food-menu/sides/sauces/sauces_Marinara.jpg',
      textButton: 'Order Now',
    },
    {
      id: 9,
      name: 'Blue Cheese',
      description:
        'A savory, creamy blue cheese dip will be a hit for game day delivery or any other occasion requiring delicious wings.',
      image: '/food-menu/sides/sauces/sauces_BlueCheese.jpg',
      textButton: 'Order Now',
    },
    {
      id: 10,
      name: 'Honey BBQ',
      description:
        'Looking for something a little sweeter on your wings or pizza? Our honey BBQ sauce is made with real honey and molasses, offering that perfect sweet and smoky flavor you crave.',
      image: '/food-menu/sides/sauces/sauces_HoneyBBQ.jpg',
      textButton: 'Order Now',
    },
    {
      id: 11,
      name: 'Garlic',
      description:
        'Garlic? Buttery parmesan? Sign us up for this flavorful wing dip. (Hint: equally delicious on pizza crust.)',
      image: '/food-menu/sides/sauces/sauces_Garlic.jpg',
      textButton: 'Order Now',
    },
    {
      id: 12,
      name: 'Icing Dip',
      description:
        'What are cinnamon sticks without icing? Or apple pies? This dip goes great on just about any of our delicious desserts, as the perfect way to end a family meal or date night dinner.',
      image: '/food-menu/sides/sauces/sauces_Icing.jpg',
      textButton: 'Order Now',
    },
  ],
};

export const elements = [
  {
    id: 1,
    name: 'PIZZAS',
    description:
      'Your favorite cheese pizza made with classic marinara sauce topped with mozzarella cheese. Order pizza online now.',
    image: '/food-menu/pizzas/popular-pizzas/pizza_Cheese.jpg',
    textButton: 'GO TO PIZZAS',
    button: {
      text: 'GO',
      link: '/',
    },
  },
  {
    id: 2,
    name: 'WINGS',
    description:
      'An order of our classic, crispy bone-in wings is sure to hit the spot. All wings come with your choice of sauce – Teriyaki, Garlic Parmesan, Honey BBQ',
    image: '/food-menu/wings/wingstreet-wings/wings_6BIWings.jpg',
    textButton: 'GO TO WINGS',
  },
  {
    id: 3,
    name: 'SIDES',
    description:
      'Stuffed with melted cheese, then finished with a buttery garlic blend and grated parmesan, our famous garlic knots are always a welcome addition to any pizza delivery order. Comes with a cup of our delicious marinara sauce!',
    image: '/food-menu/sides/sides/sides_StuffedGarlicKnots_Marinara.jpg',
    textButton: 'GO TO SIDES',
  },
  {
    id: 4,
    name: 'DESSETS',
    description:
      '10 mini cinnamon rolls, topped with signature cream cheese frosting, are the perfect way to end pizza night.',
    image: '/food-menu/desserts/Desserts_NEWCinnabonMiniRolls.jpg',
    textButton: 'GO TO DESSERTS',
  },
  {
    id: 5,
    name: 'DRINKS',
    description: 'Four Individual Sized Beverages',
    image: '/food-menu/drinks/B1_deals_5Pepsi4Pack.jpg',
    textButton: 'GO TO DRINKS',
  },
  {
    id: 6,
    name: 'PASTA',
    description:
      'Grilled chicken breast? Check. Creamy alfredo sauce? Check. Layer of cheese plus a side of breadsticks? Check and double check',
    image: '/food-menu/pasta/pasta_PanChickenAlfredo_Breadsticks.jpg',
    textButton: 'GO TO PASTA',
  },
  {
    id: 7,
    name: "P'ZONE",
    description:
      'Pepperoni and melted cheese sealed inside a folded, pizza crust. Finished with toasted parmesan and baked calzone style. Served',
    image: '/food-menu/pzone/pzone_pepperoni_producttile_175x203.jpg',
    textButton: "GO TO P'ZONE",
  },
];

export const tabsPizzas = {
  menu: [
    {
      id: 1,
      name: 'Popular Pizzas',
    },
    {
      id: 2,
      name: 'Meats',
    },
    {
      id: 3,
      name: 'Chicken',
    },
    {
      id: 4,
      name: 'Veggie',
    },
  ],
};

export const tabsWings = {
  menu: [
    {
      id: 1,
      name: 'Wingsstreet-Wings',
    },
    {
      id: 2,
      name: 'Wingsstreet-Sides',
    },
    {
      id: 3,
      name: 'Dips',
    },
  ],
};

export const tabsSides = {
  menu: [
    {
      id: 1,
      name: 'Sides',
    },
    {
      id: 2,
      name: 'Sauces',
    },
  ],
};
