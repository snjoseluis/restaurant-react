// import ApolloClient from 'apollo-boost';
// export const graphql = new ApolloClient({
//   uri: process.env.REACT_APP_API,
// });

import {
  ApolloClient,
  HttpLink,
  ApolloLink,
  InMemoryCache,
  concat,
} from '@apollo/client';

const httpLink = new HttpLink({ uri: process.env.REACT_APP_API });

const authMiddleware = new ApolloLink((operation, forward) => {
  // add the authorization to the headers
  operation.setContext({
    headers: {
      'x-token': sessionStorage.getItem('x-token') || null,
      // 'x-token': null
    },
  });

  return forward(operation);
});

export const graphql = new ApolloClient({
  cache: new InMemoryCache(),
  link: concat(authMiddleware, httpLink),
});
