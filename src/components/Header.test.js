import React from 'react';
import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import Header from './Header';

configure({ adapter: new Adapter() });

describe('Header Test', () => {
  test('That is receiving a title pizza', () => {
    const wrapper = shallow(<Header title="pizza" />);
    expect(wrapper.text()).toBe('pizza');
  });
  test('That is receiving a title', () => {
    const wrapper = shallow(<Header />);
    expect(wrapper.text()).toBe('Restaurant React');
  });
});
