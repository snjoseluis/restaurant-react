import React, { Fragment } from 'react';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles(() => ({
  header: {
    fontSize: '50px',
    margin: '25px 0px 25px 15px',
    fontWeight: 'bold',
  },
}));

const Header = ({ title }) => {
  const classes = useStyles();
  return (
    <Fragment>
      <Typography className={classes.header}>
        {title || 'Restaurant React'}
      </Typography>
    </Fragment>
  );
};

export default Header;
