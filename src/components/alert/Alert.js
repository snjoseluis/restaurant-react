import React, { Fragment } from 'react';
import { useTheme } from '@material-ui/core/styles';
import { AlertNotice, Content } from './Alert.styled';

export const Alert = ({ textAlert, transition, background }) => {
  const theme = useTheme();

  return (
    <Fragment>
      <AlertNotice
        theme={theme}
        transition={transition}
        background={background}
      >
        <Content theme={theme}>{textAlert}</Content>
      </AlertNotice>
    </Fragment>
  );
};

export default Alert;
