import styled from 'styled-components';

export const AlertNotice = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  background-color: ${(props) => props.background};
  width: 440px;
  height: 45px;
  box-shadow: 5px 5px 10px #000000;
  position: fixed;
  bottom: ${(props) => props.transition}px;
  left: 50%;
  transform: translate(-50%, -50%);
  transition: bottom 500ms ease-out;
  ${({ theme }) => theme.breakpoints.down('sm')} {
    width: 95vw;
  }
`;

export const Content = styled.div`
  margin: auto auto;
  color: #2b542c;
  font-size: 0.9rem;
`;

export const Hide = styled.button`
  background-color: transparent;
  border: none;
  margin: 10px;
  color: #50a5f4;
`;
