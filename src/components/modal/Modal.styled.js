import styled from 'styled-components';

export const Background = styled.div`
  ${({ theme }) => theme.breakpoints.up('sm')} {
    opacity: 0.7;
    backdrop-filter: blur(20px);
    background-color: #000000;
    position: fixed;
    top: 0;
    right: 0;
    width: 100vw;
    height: 100vh;
  }
`;

export const Content = styled.div`
  margin: 0 auto;
  display: flex;
  justify-content: center;
`;

export const Header = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  height: 3.5em;
  padding: 0 16px;
`;

export const HeaderIcon = styled.span`
  color: #a6a6a6;
  font-weight: bold;
  font-size: 2rem;
  text-align: right;
  cursor: pointer;
`;

export const ContentBox = styled.div`
  display: flex;
  flex-direction: column;

  background-color: #f2f2f2;
  border-radius: 5px;
  ${({ theme }) => theme.breakpoints.down('sm')} {
    height: 100vh;
    width: 100vw;
  }
`;

export const ContentContainer = styled.div`
  padding: 1em;
  ${({ theme }) => theme.breakpoints.down('sm')} {
    padding: 0;
  }
`;

const Styled = {
  Background,
  ContentBox,
  Content,
  HeaderIcon,
  Header,
  ContentContainer,
};
export default Styled;
