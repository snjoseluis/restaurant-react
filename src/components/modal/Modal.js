import React from 'react';
import Styled from './Modal.styled';

import { useTheme } from '@material-ui/core/styles';

export const Modal = ({ visible, hideModal, content, error, ...props }) => {
  const theme = useTheme();

  const handelOnClick = () => {
    hideModal();
  };
  return (
    visible && (
      <>
        <Styled.Background onClick={handelOnClick} theme={theme} />
        <Styled.ContentBox theme={theme}>
          <Styled.Header>
            <Styled.HeaderIcon onClick={handelOnClick}>
              &times;
            </Styled.HeaderIcon>
          </Styled.Header>
          <Styled.ContentContainer theme={theme}>
            <Styled.Content>{content}</Styled.Content>
          </Styled.ContentContainer>
        </Styled.ContentBox>
      </>
    )
  );
};

export default Modal;
