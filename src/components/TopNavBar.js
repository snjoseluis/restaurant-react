import React, { useState } from 'react';
import {
  AppBar,
  Toolbar,
  Typography,
  makeStyles,
  Menu,
  MenuItem,
  Hidden,
} from '@material-ui/core';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import { Link, useRouteMatch } from 'react-router-dom';
import { HamburguerMenu } from './HamburguerMenu';
import { allElements } from './../config/index';
import styled from 'styled-components';

const useStyles = makeStyles((theme) => ({
  appBar: {
    position: 'inherit',
  },
  toolbar: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  text: {
    display: 'initial',
    color: 'black',
    lineHeight: '1.5rem',
    cursor: 'pointer',
  },
  logoContainer: {
    [theme.breakpoints.down('sm')]: {
      textAlign: 'left',
      width: '75%',
    },
    [theme.breakpoints.up('sm')]: {
      textAlign: 'center',
      width: '50%',
    },
  },
  leftContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '25%',
    [theme.breakpoints.down('sm')]: {
      display: 'none',
    },
  },
  rightContainer: {
    width: '55%',
    textAlign: 'right',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  logo: {
    maxHeight: '55px',
  },
  menu: {
    position: 'relative',
    top: '200px',
  },
}));

const menu = [
  {
    title: 'DEALS',
    listItem: 'deals',
  },
  {
    title: 'MENU',
    items: allElements.menuElements,
    listItem: 'menu',
  },
  {
    title: 'ABOUT US',
    listItem: 'aboutUs',
  },
  {
    title: 'CUSTOMER SERVICE',
    listItem: 'customerService',
  },
  {
    title: 'OUR FOOD',
    listItem: 'ourFood',
  },
  {
    title: 'POLICIES',
    listItem: 'policies',
  },
];

const menuStates = {
  deals: false,
  menu: false,
  aboutUs: false,
  customerService: false,
  ourFood: false,
  policies: false,
};

const MenuItemStyked = styled(MenuItem)`
  min-width: 150px;
  a {
    text-decoration: none;
    font-weight: bold;
    color: black;
  }
  &:hover {
    background-color: #f60357;
    a {
      color: white;
    }
  }
`;

const LinkStyled = styled(Link)`
  text-decoration: none;
`;

export const TopNavBar = () => {
  const classes = useStyles();
  let [anchorEl, setAnchorEl] = useState(null);

  const handleMenuOpen = (e) => {
    setAnchorEl(e.currentTarget);
  };
  const handleMenuClose = () => {
    setAnchorEl(null);
  };

  let open = Boolean(anchorEl);

  const { path } = useRouteMatch();

  return (
    <AppBar className={classes.appBar}>
      <Toolbar className={classes.toolbar}>
        <div className={classes.leftContainer}>
          <Typography
            id="menuLink"
            className={classes.text}
            onMouseOver={handleMenuOpen}
            aria-controls="menuMenu"
            aria-haspopup="true"
            variant="h5"
          >
            Menu <KeyboardArrowDownIcon className={classes.text} />
          </Typography>
          <Menu
            id="menuMenu"
            anchorEl={anchorEl}
            keepMounted={true}
            open={open}
            onClose={handleMenuClose}
            className={classes.menu}
          >
            {allElements.menuElements.map((el, i) => (
              <MenuItemStyked key={i}>
                <Link to={el.link}>{el.name}</Link>
              </MenuItemStyked>
            ))}
          </Menu>
          <LinkStyled to="/menu/deals" underline="none">
            <Typography className={classes.text} variant="h5">
              Deals{' '}
            </Typography>
          </LinkStyled>
        </div>
        <div className={classes.logoContainer}>
          <Link to={path}>
            <Hidden xsDown>
              <img
                src="/logo.png"
                alt="Restaurant React"
                className={classes.logo}
              />
            </Hidden>
            <Hidden smUp>
              <img
                src="/logo-sm.png"
                alt="Restaurant React"
                className={classes.logo}
              />
            </Hidden>
          </Link>
        </div>
        <div className={classes.rightContainer}>
          <HamburguerMenu menu={menu} menuStates={menuStates} />
        </div>
      </Toolbar>
    </AppBar>
  );
};

export default TopNavBar;
