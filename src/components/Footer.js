import React, { Fragment } from 'react';
import Typography from '@material-ui/core/Typography';
import SiteMap from './SiteMap';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  footerContainer: {
    marginTop: '30px',
    padding: '10px',
  },
});

const Footer = () => {
  const classes = useStyles();
  return (
    <Fragment>
      <SiteMap />
      <div className={classes.footerContainer}>
        <Typography variant="caption" align="justify">
          THE DELIVERY CHARGE IS NOT A DRIVER TIP. Offers Available For A
          Limited Time. You Must Ask/Click For Certain Offers. Additional Charge
          For Extra Cheese, Stuffed Crust, Pan, And Extra Toppings May Apply.
          You Must Request Contactless And/Or Curbside Pickup. Team Members Do
          Their Best To Accommodate Contactless, Curbside And Other
          Instructions, But Availability May Vary And Is Not Guaranteed. Product
          Availability, Combinability Of Discounts And Specials, Prices,
          Participation, Delivery Areas And Charges, And Minimum Purchase
          Required For Delivery May Vary. Discounts Are Not Applicable To Tax,
          Delivery Charge, Or Driver Tip. Availability Of Fried WingStreet®
          Products And Flavors Varies By Restaurant REACT Location.
        </Typography>
      </div>
    </Fragment>
  );
};

export default Footer;
