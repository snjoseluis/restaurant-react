import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';

import {
  Link as MaterialLink,
  Drawer,
  List,
  ListItem,
  ListItemText,
  Divider,
  Collapse,
  styled,
} from '@material-ui/core';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import MenuIcon from '@material-ui/icons/Menu';

const ListItemTextStyled = styled(ListItemText)(({ theme }) => ({
  textTransform: 'uppercase',
  color: theme.palette.common.black,
}));

export const HamburguerMenuIcon = styled(MenuIcon)(({ theme }) => ({
  color: theme.palette.common.black,
  fontSize: '2.4em',
}));

export const HamburguerMenu = ({ menu, menuStates }) => {
  const [isOpen, setIsOpen] = React.useState(false);
  const [listState, setListState] = React.useState(menuStates);

  const toggleDrawer = (value) => (event) => {
    if (
      event.type === 'keydown' &&
      (event.key === 'Tab' || event.key === 'Shift')
    ) {
      return;
    }
    !value && setListState(menuStates);
    setIsOpen(value);
  };

  const toggleMenu = (listItem) => () => {
    setListState({ ...menuStates, [listItem]: !listState[listItem] });
  };

  const handleOnClick = (listItem) => () => {
    setIsOpen(false);
    toggleMenu(listItem);
  };

  const list = (menu) => (
    <div>
      <List>
        {menu.map((item, index) => (
          <Fragment key={index}>
            <ListItem button onClick={toggleMenu(item.listItem)}>
              <ListItemText primary={item.title} />
              {item.items &&
                (listState[item.listItem] ? <ExpandLess /> : <ExpandMore />)}
            </ListItem>
            {item.items && (
              <Collapse in={listState[item.listItem]}>
                <List>
                  {item.items.map((subItem, index) => (
                    <ListItem
                      key={index}
                      button
                      component={Link}
                      to={subItem.link}
                      onClick={handleOnClick(item.listItem)}
                    >
                      <ListItemTextStyled primary={subItem.title} />
                    </ListItem>
                  ))}
                </List>
              </Collapse>
            )}
            <Divider />
          </Fragment>
        ))}
      </List>
    </div>
  );

  return (
    <Fragment>
      <MaterialLink onClick={toggleDrawer(true)}>
        <HamburguerMenuIcon />
      </MaterialLink>
      <Drawer open={isOpen} anchor="right" onClose={toggleDrawer(false)}>
        {list(menu)}
      </Drawer>
    </Fragment>
  );
};
