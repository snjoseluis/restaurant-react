import React, { Fragment, useContext, useState, useEffect } from 'react';
import * as styles from './index';
import { MdClose } from 'react-icons/md';
import { ModalContext } from '../../contexts/ModalContext';
import { CartContext } from '../../contexts/CartContext';

export const Cart = () => {
  const modalContext = useContext(ModalContext);
  const cartContext = useContext(CartContext);
  const productsList = cartContext.cart;
  const [total, setTotal] = useState(0);

  useEffect(() => {
    let totalSale = productsList.reduce((prev, curr) => prev + curr.price, 0);
    setTotal(totalSale);
  }, [total]);

  const handleModalClose = () => {
    modalContext.hideModal();
  };

  return (
    <Fragment>
      <styles.Modal>
        <styles.HeaderSection>
          <styles.ClosingCross onClick={handleModalClose}>
            <MdClose />
          </styles.ClosingCross>
        </styles.HeaderSection>
        <div>
          <styles.HrDivsion />
          <styles.ListContainer>
            <styles.TableHead>
              <h3>Product</h3>
              <h3>Price</h3>
            </styles.TableHead>
            {productsList.map((product, key) => {
              return (
                <styles.TableBody key={key}>
                  <styles.ProductDesc>
                    {product.name.slice(0, 35)}
                  </styles.ProductDesc>
                  <styles.ProductPrice>
                    <span>$</span>
                    {product.price}
                  </styles.ProductPrice>
                </styles.TableBody>
              );
            })}
            {total <= 0 ? (
              <p data-test="empty-cart-component">Your Cart Is Empty</p>
            ) : (
              <styles.TotalProduct>
                <styles.TotalText>total: </styles.TotalText>
                <styles.Total>
                  <span>$</span>
                  {total}
                </styles.Total>
              </styles.TotalProduct>
            )}
          </styles.ListContainer>
        </div>
        <styles.CheckoutSection>
          <styles.CheckoutButton data-test="checkout-button">
            checkout
          </styles.CheckoutButton>
        </styles.CheckoutSection>
      </styles.Modal>
    </Fragment>
  );
};
