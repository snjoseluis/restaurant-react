import React from 'react';
import { shallow } from 'enzyme';
import { Cart } from './Cart';

const setup = (props) => {
  return shallow(<Cart {...props} />);
};

describe('testing cart component and its functionality', () => {
  test('the text on checkout button should be lowercase', () => {
    const wrapper = setup({ total: 0 });
    const button = wrapper.find("[data-test='checkout-button']").text();
    expect(button).toBe('checkout');
  });

  test('renders "Your Cart Is Empty" when no selected Item', () => {
    const wrapper = setup({ total: 0 });
    const component = wrapper.find("[data-test='empty-cart-component']");
    expect(component.text()).toBe('Your Cart Is Empty');
  });
});
