import styled from 'styled-components';
import { globalStyles } from '../../global';
import { Button } from '../../base/button';

export const Modal = styled.div`
  background-color: white;
  width: 400px;
  height: 100vh;
  position: fixed;
  top: 0;
  right: 0;
  display: flex;
  flex-direction: column;
  align-content: space-between;

  @media (max-width: 600px) {
    width: 100%;
  }
`;

export const HrDivsion = styled.hr`
  border: 0.3px solid ${globalStyles.color.lines};
  width: 100%;
`;

export const HeaderSection = styled.div`
  margin: 10px;
`;

export const ClosingCross = styled.a`
  float: right;
  font-size: 1.9rem;
  color: ${globalStyles.color.disabled};
  cursor: pointer;
`;

export const CheckoutButton = styled(Button)`
  width: 100%;
  height: 100%;
  font-weight: 500;
  font-size: 1.2rem;
`;

export const CheckoutSection = styled.div`
  position: absolute;
  bottom: 0;
  height: 70px;
  width: 100%;
`;

export const ListContainer = styled.div`
  height: 100%;
  margin: 15px;
`;

export const TableHead = styled.div`
  display: grid;
  grid-template-columns: 70% 30%;
  grid-gap: 20px;
`;

export const TableBody = styled.div`
  display: grid;
  grid-template-columns: 70% 30%;
  grid-gap: 20px;
`;

export const ProductDesc = styled.p`
  font-size: 0.8rem;
  font-weight: bold;
  box-sizing: border-box;
  padding: 0;
  border-bottom: 2px solid black;
`;

export const ProductPrice = styled.p`
  font-size: 0.8rem;
  font-weight: bold;
  display: inline-block;
`;

export const TotalProduct = styled.div`
  display: grid;
  grid-template-columns: 70% 30%;
  grid-gap: 20px;
`;

export const TotalText = styled.p`
  font-size: 0.8rem;
  font-weight: bold;
  margin-right: 10px;
  justify-self: flex-end;
  text-transform: uppercase;
`;

export const Total = styled.p`
  font-size: 0.8rem;
  font-weight: bold;
`;
