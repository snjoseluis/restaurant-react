import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core';
import Typography from '@material-ui/core/Typography';
import { Link } from 'react-router-dom';

const useStyles = makeStyles(() => ({
  footerElements: {
    listStyle: 'none',
    padding: '0',
    margin: '0',
  },
  links: {
    textDecoration: 'none',
    color: 'black',
  },
  elements: {
    fontSize: '12px',
  },
  elementsHeader: {
    fontSize: '15px',
    fontWeight: 'bold',
  },
}));

const SiteMapElement = ({ elements, title }) => {
  const classes = useStyles();

  return (
    <Fragment>
      <div>
        <Typography className={classes.elementsHeader} align="left">
          {title}
        </Typography>
        {elements.map((element, index) => {
          return (
            <ul className={classes.footerElements} key={index}>
              <Link to={element.link} className={classes.links}>
                <li>
                  <Typography className={classes.elements} align="left">
                    {element.name}
                  </Typography>
                </li>
              </Link>
            </ul>
          );
        })}
      </div>
    </Fragment>
  );
};

export default SiteMapElement;
