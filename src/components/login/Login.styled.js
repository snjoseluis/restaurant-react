import { Paper, Button } from '@material-ui/core';
import { globalStyles } from '../../global';
import styled from 'styled-components';

export const TransitionContainer = styled.div`
  width: 515px;
  height: 465px;
  margin: 0 auto;
  position: fixed;
  top: 20%;
  overflow-x: hidden;
  ${({ theme }) => theme.breakpoints.down('sm')} {
    width: 100vw;
    height: 100vh;
  }
`;

export const Overflow = styled.div`
  display: flex;
  width: 1100px;
  position: relative;
  left: ${(props) => props.transition}vw;
  ${({ theme }) => theme.breakpoints.down('sm')} {
    left: ${(props) => (props.transition === 0 ? `0` : `-100`)}vw;
  }
  ${({ theme }) => theme.breakpoints.up('sm')} {
    left: ${(props) => (props.transition === 0 ? `0` : `-527`)}px;
  }
  transition: left 500ms ease-out;
`;

export const DivContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-self: center;
  width: 515px;
  height: 460px;
  margin: 2px;
  ${({ theme }) => theme.breakpoints.down('sm')} {
    width: 98vw;
    height: 95vh;
  }
`;

export const CustomPaper = styled(Paper)`
  width: 521px;
  height: 460px;
  display: flex;
  flex-direction: column;
  align-items: center;
  padding-top: 50px;
  ${({ theme }) => theme.breakpoints.down('sm')} {
    width: 100vw;
    height: 94vh;
    justify-content: center;
  }
`;

export const Title = styled.h1`
  height: 52px;
  font-size: 3rem;
  font-weight: 800;
  text-align: center;
  color: ${globalStyles.color.fontBase};
  margin-bottom: 42px;
  margin-top: 0;
  ${({ theme }) => theme.breakpoints.down('sm')} {
    font-size: 2.7rem;
  }
`;

export const DivForm = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  width: 50%;
  position: relative;
  z-index: 1;
  ${({ theme }) => theme.breakpoints.down('sm')} {
    width: 88%;
  }
`;

export const HrDivsion = styled.hr`
  border: 0.3px solid ${globalStyles.color.lines};
  width: 100%;
`;

export const LinkTo = styled.p`
  color: #f60357;
  text-align: center;
  cursor: pointer;
  font-size: 15px;
  font-weight: 600;
`;

export const CustomButton = styled(Button)`
  margin: 25px 0px;
  padding: 10px;
  border-radius: 10px;
  text-align: center;
  font-size: 15px;
  font-weight: 600;
`;

export const Form = styled.form`
  display: flex;
  flex-direction: column;
`;

export const ProgressiveBar = styled.div`
  height: 5px;
  background-color: #f60357;
  animation-name: progressiveBar;
  animation-duration: 1500ms;
  animation-iteration-count: infinite;
  @keyframes progressiveBar {
    0% {
      width: 0%;
    }
    50% {
      width: 100%;
    }
    100% {
      width: 0%;
    }
  }
`;

export const GrayScreen = styled.div`
  width: 515px;
  height: 458px;
  background-color: #ebecf0;
  opacity: 0.5;
  position: absolute;
  z-index: 10000;
`;

const Styled = {
  DivContainer,
  CustomPaper,
  Title,
  DivForm,
  HrDivsion,
  LinkTo,
  CustomButton,
  TransitionContainer,
  Overflow,
  ProgressiveBar,
  GrayScreen,
};

export default Styled;
