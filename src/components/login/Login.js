import React, { Fragment, useState, useContext } from 'react';

import { Link } from '@material-ui/core';
import TextField from '@material-ui/core/TextField';

import Styled from './Login.styled';
import { SessionContext } from '../../contexts/SessionContext/SessionContext';
import { ModalContext } from '../../contexts/ModalContext/ModalContext';
import { AlertContext } from '../../contexts/AlertContext';
import { responseCodes } from '../../const';

import { useTheme } from '@material-ui/core/styles';

const EMAIL_POSITION = 0;
const PASSWORD_POSITION = 1;

export const Login = ({ userMutation }) => {
  const [buttonDisabled, setButtonDisabled] = useState(true);
  const [buttonDisabledPsw, setButtonDisabledPsw] = useState(true);
  const [transition, setTransition] = useState(EMAIL_POSITION);
  const [isLoading, setIsLoading] = useState(false);
  let email, password;
  const [greeting, setGreeting] = useState('Welcome!');

  const sessionContext = useContext(SessionContext);
  const modalContext = useContext(ModalContext);
  const alertContext = useContext(AlertContext);
  const theme = useTheme();

  const handleOnChange = (e) => {
    const reference = e.target.value.trim();
    const emailFormat = /^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/;
    if (reference.match(emailFormat)) {
      setButtonDisabled(false);
    } else {
      setButtonDisabled(true);
    }
  };

  const handleNextButton = async (e) => {
    setIsLoading(true);
    e.preventDefault();
    const result = await sessionContext.findUser(email.value);
    if (result === responseCodes.USER_FOUND) {
      setGreeting('Welcome Back!');
    } else if (result === responseCodes.USER_CREATED) {
      setGreeting('Welcome!');
    }
    if (result) {
      setIsLoading(false);
      setTransition(PASSWORD_POSITION);
    }
  };

  const handleLoginButton = async (e) => {
    e.preventDefault();
    setIsLoading(true);
    const result = await sessionContext.login(email.value, password.value);
    console.log(result);
    if (result === responseCodes.USER_LOGIN) {
      modalContext.hideModal();
    } else if (result === responseCodes.INCORRECT_PASSWORD) {
      alertContext.showAlert();
      setIsLoading(false);
    }
  };

  const handleOnChangePassword = (e) => {
    e.preventDefault();
    if (e.target.value.length > 8) {
      setButtonDisabledPsw(false);
    } else {
      setButtonDisabledPsw(true);
    }
  };

  const handleBackToLogin = () => {
    setTransition(0);
  };

  return (
    <Fragment>
      <Styled.TransitionContainer theme={theme}>
        <Styled.Overflow transition={transition} theme={theme}>
          <Styled.DivContainer theme={theme}>
            {isLoading && <Styled.ProgressiveBar />}
            <Styled.CustomPaper theme={theme}>
              <Styled.Title theme={theme}>Log in</Styled.Title>
              <Styled.DivForm theme={theme}>
                <TextField
                  label="Email"
                  type="text"
                  name="email"
                  inputRef={(ref) => (email = ref)}
                  onChange={handleOnChange}
                />
                <Styled.CustomButton
                  variant="contained"
                  color="secondary"
                  type="submit"
                  id="signInButton"
                  disabled={buttonDisabled || isLoading}
                  onClick={handleNextButton}
                >
                  Next
                </Styled.CustomButton>
                <Link>
                  <Styled.LinkTo>Forgot login Info?</Styled.LinkTo>
                </Link>
                <Styled.HrDivsion />
              </Styled.DivForm>
            </Styled.CustomPaper>
            {isLoading && <Styled.GrayScreen />}
          </Styled.DivContainer>
          <Styled.DivContainer theme={theme}>
            <Styled.CustomPaper theme={theme}>
              {isLoading && <Styled.ProgressiveBar />}
              <Styled.Title theme={theme}>{greeting}</Styled.Title>
              <Styled.DivForm theme={theme}>
                <TextField
                  label="Password"
                  type="password"
                  name="password"
                  inputRef={(ref) => (password = ref)}
                  onChange={handleOnChangePassword}
                />
                <Styled.CustomButton
                  variant="contained"
                  color="secondary"
                  type="submit"
                  id="logInButton"
                  disabled={buttonDisabledPsw}
                  onClick={handleLoginButton}
                >
                  Log In
                </Styled.CustomButton>
                <Link>
                  <Styled.LinkTo>Forgot Password?</Styled.LinkTo>
                </Link>
                <Styled.HrDivsion />
                <Link>
                  <Styled.LinkTo onClick={handleBackToLogin}>
                    Back to log in
                  </Styled.LinkTo>
                </Link>
                <Styled.HrDivsion />
              </Styled.DivForm>
            </Styled.CustomPaper>
            {isLoading && <Styled.GrayScreen />}
          </Styled.DivContainer>
        </Styled.Overflow>
      </Styled.TransitionContainer>
    </Fragment>
  );
};

export default Login;
