import React, { Fragment, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Popover from '@material-ui/core/Popover';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  paper: {
    border: '1px solid #CFCECC',
    borderRadius: '5px',
    padding: '10px',
    display: 'inline-block',
    width: '250px',
  },
  anchor: {
    cursor: 'pointer',
    textAlign: 'center',
    fontSize: '14px',
    display: 'inline-block',
  },
  popover: {
    pointerEvents: 'none',
  },
}));

const PopUp = ({ description }) => {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = useState(null);

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  return (
    <Fragment>
      <span>{description.slice(0, 114)}</span>
      <Typography
        aria-owns={open ? 'mouse-over-popover' : undefined}
        aria-haspopup="true"
        onMouseEnter={handlePopoverOpen}
        onMouseLeave={handlePopoverClose}
        className={classes.anchor}
      >
        . More...
      </Typography>
      <Popover
        id="mouse-over-popover"
        className={classes.popover}
        classes={{
          paper: classes.paper,
        }}
        open={open}
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        transformOrigin={{
          vertical: 'top',
          horizontal: 'left',
        }}
        onClose={handlePopoverClose}
        disableRestoreFocus
      >
        <Typography>{description}</Typography>
      </Popover>
    </Fragment>
  );
};

export default PopUp;
