import styled from 'styled-components';
import { BiShoppingBag } from 'react-icons/bi';

export const ShoppingCartIcon = styled(BiShoppingBag)`
  font-size: 30px;
`;

export const ButtonStyled = styled.a`
  background-color: #333333;
  cursor: pointer;
`;

export const CartIcon = styled.div`
  background-color: #bb0202;
  margin: 0px;
  padding: 0;
  border-radius: 100%;
  width: 20px;
  text-align: center;
  font-size: 0.9rem;
  display: inline-block;
  position: relative;
  left: -7px;
  top: -21px;
`;
