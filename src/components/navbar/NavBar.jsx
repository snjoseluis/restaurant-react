import React, { useContext } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import { makeStyles, Hidden } from '@material-ui/core';

import * as styles from './index';

import { ModalContext } from './../../contexts/ModalContext';
import { SessionContext } from '../../contexts/SessionContext';
import { CartContext } from '../../contexts/CartContext';
import { Login } from './../login';
import { Cart } from '../cart/Cart';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  appBar: {
    background: '#333333',
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    textAlign: 'left',
  },
  TypographyP: {
    display: 'inline-block',
  },
}));

export const NavBar = () => {
  const classes = useStyles();
  const modalContext = useContext(ModalContext);
  const sessionContext = useContext(SessionContext);
  const cartContext = useContext(CartContext);
  const cartCount = cartContext.cartCount;
  const loginContent = <Login />;
  const cartContent = <Cart />;

  const handleOnSignin = () => {
    modalContext.showModal();
    modalContext.setModalContent(loginContent);
  };

  const handleOnLogout = () => {
    sessionContext.logout();
  };

  const handleClickCartIcon = () => {
    modalContext.showModal();
    modalContext.setModalContent(cartContent);
  };

  return (
    <AppBar className={classes.appBar} position="static">
      <Toolbar>
        <Hidden xsDown>
          <Typography variant="h6" className={classes.title}>
            Welcome to Restaurant App!
          </Typography>
        </Hidden>
        <Hidden smUp>
          <Typography variant="h6" className={classes.title}>
            Welcome!
          </Typography>
        </Hidden>
        <div>
          {sessionContext.isLoggedIn ? (
            <Hidden smDown>
              <Typography className={classes.TypographyP}>Welcome!!</Typography>
              &nbsp;|&nbsp;
              <styles.ButtonStyled onClick={handleOnLogout}>
                Logout
              </styles.ButtonStyled>
            </Hidden>
          ) : (
            <Hidden smDown>
              <styles.ButtonStyled onClick={handleOnSignin}>
                Sign In
              </styles.ButtonStyled>
              &nbsp;|&nbsp;
            </Hidden>
          )}
          <styles.ButtonStyled onClick={handleClickCartIcon}>
            <styles.ShoppingCartIcon />
            <styles.CartIcon>
              <span>{cartCount}</span>
            </styles.CartIcon>
          </styles.ButtonStyled>
        </div>
      </Toolbar>
    </AppBar>
  );
};
