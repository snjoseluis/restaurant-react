import React, { useContext } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';
import { Button } from '../base/button';
import { Skeleton } from '@material-ui/lab';
import PopUp from './PopUp';
import { CartContext } from '../contexts/CartContext';
import styled from 'styled-components';

const useStyles = makeStyles({
  cardContent: {
    padding: '15px',
  },
  div: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  divContent: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    width: '80%',
    minHeight: '149px',
  },
  divContentSkeleton: {
    display: 'flex',
    flexDirection: 'column',
    width: '90%',
    marginTop: '20px',
  },
  h2: {
    textAlign: 'left',
    fontSize: '21px',
  },
  typography: {
    textAlign: 'left',
    fontSize: '14px',
    maxWidth: '90%',
  },
  buttonSkeleton: {
    marginTop: '35px',
    borderRadius: '10px',
    maxWidth: '80%',
    padding: '15px',
  },
  span: {
    fontSize: '18px',
  },
  img: {
    borderRadius: '5px',
    maxHeight: '135px',
    maxWidth: '135px',
  },
});

const Description = ({ description, typography }) => {
  return (
    <Typography variant="subtitle2" className={typography}>
      {description.length > 114 ? (
        <PopUp description={description} />
      ) : (
        description
      )}
    </Typography>
  );
};

const AddButton = styled(Button)`
  padding: 15px;
  border-radius: 10px;
  width: 90%;
`;

const ItemButton = ({ id, name, price }) => {
  const cartContext = useContext(CartContext);

  const handleOnAddToCart = (id, name, price) => {
    cartContext.addProduct(id, name, price);
    console.log([id, name, price]);
  };

  return (
    <AddButton onClick={() => handleOnAddToCart(id, name, price)}>
      ADD TO CART
    </AddButton>
  );
};

const ItemCard = ({
  name,
  description,
  textButton,
  image,
  price,
  id,
  query,
  ...props
}) => {
  const classes = useStyles();

  return (
    <Card>
      <CardContent className={classes.cardContent}>
        <h2 variant="subtitle1" className={classes.h2}>
          {name}
        </h2>
        <div className={classes.div}>
          <div className={classes.divContent}>
            <Description
              description={description}
              typography={classes.typography}
            />
            <ItemButton
              classButton={classes.button}
              classSpan={classes.span}
              textButton={textButton}
              id={id}
              price={price}
              name={name}
              description={description}
              image={image}
            />
          </div>
          <div>
            <img src={image} alt="Item" className={classes.img} />
          </div>
        </div>
      </CardContent>
    </Card>
  );
};

export const ItemCardSkeleton = (props) => {
  const classes = useStyles();

  return (
    <div className={classes.cardContent}>
      <Skeleton variant="text" height="36px" width="45%" />
      <div className={classes.div} width="1000%">
        <div className={classes.divContentSkeleton}>
          <div>
            <Skeleton variant="text" className={classes.typography} />
            <Skeleton variant="text" className={classes.typography} />
            <Skeleton variant="text" className={classes.typography} />
          </div>
          <div>
            <Skeleton
              variant="rect"
              className={classes.buttonSkeleton}
              height="20px"
              width="90%"
            />
          </div>
        </div>
        <div>
          <Skeleton
            variant="rect"
            className={classes.img}
            height="100px"
            width="100px"
          />
        </div>
      </div>
    </div>
  );
};

export default ItemCard;
