import React, { Fragment } from 'react';
import { Grid } from '@material-ui/core';
import ItemCard, { ItemCardSkeleton } from './ItemCard';
import { useQuery } from '@apollo/react-hooks';

const ItemsSkeleton = () => (
  <Fragment>
    {Array(2)
      .fill(<ItemCardSkeleton />)
      .map((el, i) => (
        <Grid item xs={12} lg={6} key={i}>
          {el}
        </Grid>
      ))}
  </Fragment>
);

const ItemContainer = ({ query }) => {
  const { data, error, loading } = useQuery(query);
  if (loading) {
    return (
      <Grid container>
        <ItemsSkeleton />
      </Grid>
    );
  }

  if (error) {
    return (
      <Grid container spacing={1}>
        <p>Sorry!! Something when wrong!!</p>
      </Grid>
    );
  }

  if (data) {
    return (
      <Grid container spacing={1}>
        {data.products.map((item) => {
          return (
            <Grid item xs={12} lg={6} key={item.id}>
              {
                <ItemCard
                  name={item.name}
                  price={item.price}
                  description={item.description}
                  image={item.images[0].url}
                  textButton={item.textButton}
                  id={item.id}
                  query={query}
                />
              }
            </Grid>
          );
        })}
      </Grid>
    );
  }
};

export default ItemContainer;
