import React, { Fragment } from 'react';
import { makeStyles, Hidden } from '@material-ui/core';
import SiteMapElement from './SiteMapElements';
import { allElements } from './../config/index';

const useStyles = makeStyles(() => ({
  footerContainer: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: '100px',
    marginBottom: '30px',
  },
  logo: {
    maxHeight: '55px',
  },
}));

const SiteMap = () => {
  const classes = useStyles();

  return (
    <Fragment>
      <Hidden smDown>
        <div className={classes.footerContainer}>
          <SiteMapElement title="MENU" elements={allElements.menuElements} />
          <SiteMapElement title="OUR FOOD" elements={allElements.ourFoodEle} />
          <SiteMapElement title="ABOUT US" elements={allElements.aboutUs} />
          <SiteMapElement
            title="COSTUMER SERVICE"
            elements={allElements.costumerService}
          />
          <SiteMapElement title="MY ACCOUNT" elements={allElements.myAccount} />
        </div>
      </Hidden>
    </Fragment>
  );
};

export default SiteMap;
