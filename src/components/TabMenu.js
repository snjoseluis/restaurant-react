import React, { Fragment } from 'react';
import { makeStyles } from '@material-ui/core/styles';

import { Paper, Tabs } from '@material-ui/core';
import Tab from '@material-ui/core/Tab';

const useStyles = makeStyles({
  paper: {
    margin: '10px 0px',
  },
});

const TabMenu = ({ activeTab, handleChange, menuOptions, title }) => {
  const classes = useStyles();
  return (
    <Fragment>
      <Paper className={classes.paper}>
        <Tabs
          value={activeTab}
          indicatorColor="secondary"
          centered
          onChange={handleChange}
        >
          {menuOptions.menu.map((menuItem) => (
            <Tab label={menuItem.name} key={menuItem.id} />
          ))}
        </Tabs>
      </Paper>
    </Fragment>
  );
};

export default TabMenu;
