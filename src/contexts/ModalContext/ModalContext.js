import React, { useState, createContext } from 'react';
import Modal from '../../components/modal/Modal';

export const ModalContext = createContext();

export const ModalProvider = ({ children }) => {
  const [visible, setVisible] = useState(false);
  const [content, setContent] = useState();

  const showModal = () => {
    setVisible(true);
  };

  const hideModal = () => {
    setVisible(false);
  };

  const setModalContent = (value) => {
    setContent(value);
  };

  return (
    <ModalContext.Provider value={{ showModal, hideModal, setModalContent }}>
      {children}
      <Modal visible={visible} hideModal={hideModal} content={content} />
    </ModalContext.Provider>
  );
};

export default ModalProvider;
