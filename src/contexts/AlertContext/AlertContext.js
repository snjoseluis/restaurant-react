import React, { createContext, useState } from 'react';
import { Alert } from './../../components/alert/Alert';

export const AlertContext = createContext();

export const AlertProvider = ({ children }) => {
  const [transition, setTransition] = useState(-70);
  const background = '#DFF0D8';

  const showAlert = () => {
    setTransition(-15);
    setTimeout(() => {
      setTransition(-70);
    }, 3000);
  };

  return (
    <AlertContext.Provider value={{ showAlert }}>
      {children}
      <Alert
        transition={transition}
        textAlert="Incorrect Password! Please try again!"
        background={background}
      />
    </AlertContext.Provider>
  );
};

export default AlertProvider;
