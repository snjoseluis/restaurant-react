import React, { createContext, useState, useEffect } from 'react';

export const CartContext = createContext();
export const CartConsumer = CartContext.Consumer;

export const CartContextProvider = ({ children }) => {
  const [cart, setCart] = useState(
    JSON.parse(localStorage.getItem('cart')) || []
  );
  const cartCount = cart.length;

  const addProduct = (id, name, price) => {
    let CartItem;
    setCart([
      (CartItem = {
        id,
        name,
        price,
      }),
      ...cart,
    ]);
  };

  useEffect(() => {
    if (cart.length >= 1) {
      localStorage.setItem('cart', JSON.stringify(cart));
    }
  }, [cart]);

  const removeProduct = () => {};

  return (
    <CartContext.Provider
      value={{ addProduct, removeProduct, cart, cartCount }}
    >
      {children}
    </CartContext.Provider>
  );
};

export default CartContextProvider;
