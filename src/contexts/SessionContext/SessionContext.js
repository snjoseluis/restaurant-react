import React, { useState } from 'react';
import gql from 'graphql-tag';
import { useMutation } from '@apollo/react-hooks';
import { responseCodes } from '../../const';

const SEARCH_USER = gql`
  mutation findUserMutation($email: String!) {
    findUser(email: $email) {
      token
      code
    }
  }
`;

const LOGIN_USER = gql`
  mutation loginMutation($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      token
      code
    }
  }
`;

const getUser = () => {
  console.log('getUser');
};

export const SessionContext = React.createContext();
export const SessionConsumer = SessionContext.Consumer;

export const SessionContextProvider = ({ children }) => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const [findUserMutation] = useMutation(SEARCH_USER);
  const [loginMutation] = useMutation(LOGIN_USER);

  const findUser = async (email) => {
    await new Promise((r) => setTimeout(r, 3000));
    const result = await findUserMutation({ variables: { email } });
    sessionStorage.setItem('x-token', result.data.findUser.token);
    return result.data.findUser.code;
  };

  const login = async (email, password) => {
    try {
      await new Promise((r) => setTimeout(r, 3000));
      const result = await loginMutation({ variables: { email, password } });
      console.log(result);
      if (result.data.login.code === responseCodes.USER_LOGIN) {
        sessionStorage.setItem('x-token', result.data.login.token);
        sessionStorage.setItem('code', result.data.login.code);
        setIsLoggedIn(true);
      }
      return result.data.login.code;
    } catch (err) {
      console.log(err);
      return err;
    }
  };

  const logout = () => {
    setIsLoggedIn(false);
    sessionStorage.removeItem('x-token');
  };

  return (
    <SessionContext.Provider
      value={{ isLoggedIn, getUser, findUser, login, logout }}
    >
      {children}
    </SessionContext.Provider>
  );
};

export default SessionContextProvider;
