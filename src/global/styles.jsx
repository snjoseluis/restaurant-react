export const globalStyles = {
  background: {
    primary: '#f50057',
    disabled: '#EEEEEE',
  },
  typography: {},
  color: {
    fontBase: '#000000',
    lines: '#DDDDDD',
    disabled: '#666666',
  },
};
