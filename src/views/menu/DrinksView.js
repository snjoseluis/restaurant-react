import React, { Fragment } from 'react';
import ItemContainer from '../../components/ItemContainer';
import Header from '../../components/Header';
import propTypes from './PropTypes';

import { gql } from 'apollo-boost';

const drinks = gql`
  {
    products(category: "drinks") {
      id
      name
      description
      price
      images {
        url
      }
    }
  }
`;

const DrinksView = ({ items, tabs, title, ...props }) => {
  return (
    <Fragment>
      <Header title="DRINKS" />
      <ItemContainer query={drinks} />
    </Fragment>
  );
};

DrinksView.propTypes = propTypes;

export default DrinksView;
