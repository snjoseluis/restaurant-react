import React, { Fragment } from 'react';
import Header from '../../components/Header';
import ItemContainer from '../../components/ItemContainer';
import propTypes from './PropTypes';

import { gql } from 'apollo-boost';

const pZone = gql`
  {
    products(category: "pZone") {
      id
      name
      description
      price
      images {
        url
      }
    }
  }
`;

const PzoneView = ({ items, title, ...props }) => {
  return (
    <Fragment>
      <Header title="P'ZONE" />
      <ItemContainer query={pZone} />
    </Fragment>
  );
};

PzoneView.propTypes = propTypes;

export default PzoneView;
