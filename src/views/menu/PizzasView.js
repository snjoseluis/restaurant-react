import React, { Fragment } from 'react';
import { gql } from 'apollo-boost';

import ItemContainer from '../../components/ItemContainer';
import PropTypes from 'prop-types';
import propTypes from './PropTypes';

const popularPizzas = gql`
  {
    products(category: "pizza", subCategory: "popular-pizzas") {
      id
      name
      description
      price
      images {
        url
      }
    }
  }
`;

const meatsPizzas = gql`
  {
    products(category: "pizza", subCategory: "meats") {
      id
      name
      description
      price
      images {
        url
      }
    }
  }
`;

const chickenPizzas = gql`
  {
    products(category: "pizza", subCategory: "chicken") {
      id
      name
      description
      price
      images {
        url
      }
    }
  }
`;

const veggiePizzas = gql`
  {
    products(category: "pizza", subCategory: "veggie") {
      id
      name
      description
      price
      images {
        url
      }
    }
  }
`;

const PizzasView = ({ activeTab }) => {
  const TabPanel = (props) => {
    const { children, value, index, ...other } = props;
    return (
      <div
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && <div>{children}</div>}
      </div>
    );
  };

  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };

  return (
    <Fragment>
      <TabPanel value={activeTab} index={0}>
        <ItemContainer query={popularPizzas} />
      </TabPanel>
      <TabPanel value={activeTab} index={1}>
        <ItemContainer query={meatsPizzas} />
      </TabPanel>
      <TabPanel value={activeTab} index={2}>
        <ItemContainer query={chickenPizzas} />
      </TabPanel>
      <TabPanel value={activeTab} index={3}>
        <ItemContainer query={veggiePizzas} />
      </TabPanel>
    </Fragment>
  );
};

PizzasView.propTypes = propTypes;

export default PizzasView;
