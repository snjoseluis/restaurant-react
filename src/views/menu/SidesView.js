import React, { Fragment } from 'react';
import ItemContainer from '../../components/ItemContainer';
import PropTypes from 'prop-types';
import propTypes from './PropTypes';

import { gql } from 'apollo-boost';

const sides = gql`
  {
    products(category: "sides", subCategory: "sides") {
      id
      name
      description
      price
      images {
        url
      }
    }
  }
`;

const sauces = gql`
  {
    products(category: "sides", subCategory: "sauces") {
      id
      name
      description
      price
      images {
        url
      }
    }
  }
`;

const SidesView = (props) => {
  const TabPanel = (props) => {
    const { children, value, index, ...other } = props;
    return (
      <div
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && <div>{children}</div>}
      </div>
    );
  };

  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };
  return (
    <Fragment>
      <TabPanel value={props.activeTab} index={0}>
        <ItemContainer query={sides} />
      </TabPanel>
      <TabPanel value={props.activeTab} index={1}>
        <ItemContainer query={sauces} />
      </TabPanel>
    </Fragment>
  );
};

SidesView.propTypes = propTypes;

export default SidesView;
