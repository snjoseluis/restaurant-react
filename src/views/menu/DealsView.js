import React, { Fragment } from 'react';
import Header from '../../components/Header';
import ItemContainer from '../../components/ItemContainer';

import { gql } from 'apollo-boost';

const deals = gql`
  {
    products(category: "deals") {
      id
      name
      description
      price
      images {
        url
      }
    }
  }
`;

const DealsView = ({ items, tabs }) => {
  return (
    <Fragment>
      <Header title="DEALS" />
      <ItemContainer query={deals} />
    </Fragment>
  );
};

export default DealsView;
