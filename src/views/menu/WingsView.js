import React, { Fragment } from 'react';
import ItemContainer from '../../components/ItemContainer';
import propTypes from './PropTypes';
import PropTypes from 'prop-types';

import { gql } from 'apollo-boost';

const wingsStreet = gql`
  {
    products(category: "wings", subCategory: "wingstreetWings") {
      id
      name
      description
      price
      images {
        url
      }
    }
  }
`;

const wingsSides = gql`
  {
    products(category: "wings", subCategory: "wingstreetSides") {
      id
      name
      description
      price
      images {
        url
      }
    }
  }
`;

const dips = gql`
  {
    products(category: "wings", subCategory: "dips") {
      id
      name
      description
      price
      images {
        url
      }
    }
  }
`;

const WingsView = (props) => {
  const TabPanel = (props) => {
    const { children, value, index, ...other } = props;
    return (
      <div
        component="div"
        role="tabpanel"
        hidden={value !== index}
        id={`simple-tabpanel-${index}`}
        aria-labelledby={`simple-tab-${index}`}
        {...other}
      >
        {value === index && <div>{children}</div>}
      </div>
    );
  };

  TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.any.isRequired,
    value: PropTypes.any.isRequired,
  };
  return (
    <Fragment>
      <TabPanel value={props.activeTab} index={0}>
        <ItemContainer query={wingsStreet} />
      </TabPanel>
      <TabPanel value={props.activeTab} index={1}>
        <ItemContainer query={wingsSides} />
      </TabPanel>
      <TabPanel value={props.activeTab} index={2}>
        <ItemContainer query={dips} />
      </TabPanel>
    </Fragment>
  );
};

WingsView.propTypes = propTypes;

export default WingsView;
