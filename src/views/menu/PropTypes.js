import PropTypes from 'prop-types';

export default {
  tabs: PropTypes.array,
  items: PropTypes.any.isRequired,
  title: PropTypes.any,
  activeTab: PropTypes.any,
};
