import React, { Fragment } from 'react';
import propTypes from './PropTypes';
import Header from '../../components/Header';
import ItemContainer from '../../components/ItemContainer';

import { gql } from 'apollo-boost';

const desserts = gql`
  {
    products(category: "desserts") {
      id
      name
      description
      price
      images {
        url
      }
    }
  }
`;

const DessertsView = ({ items, tabs }) => {
  return (
    <Fragment>
      <Header title="DESSERTS" />
      <ItemContainer query={desserts} />
    </Fragment>
  );
};

DessertsView.propTypes = propTypes;

export default DessertsView;
