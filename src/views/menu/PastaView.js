import React, { Fragment } from 'react';
import Header from '../../components/Header';
import propTypes from './PropTypes';
import ItemContainer from '../../components/ItemContainer';

import { gql } from 'apollo-boost';

const pastas = gql`
  {
    products(category: "pasta") {
      id
      name
      description
      price
      images {
        url
      }
    }
  }
`;

const PastaView = ({ items, title, ...props }) => {
  return (
    <Fragment>
      <Header title="PASTA" />
      <ItemContainer query={pastas} />
    </Fragment>
  );
};

PastaView.propTypes = propTypes;

export default PastaView;
