import React, { Fragment } from 'react';
import { useRouteMatch, Route } from 'react-router-dom';
import WingsView from './menu/WingsView';
import PizzasView from './menu/PizzasView';
import DrinksView from './menu/DrinksView';
import TabMenu from '../components/TabMenu';
import DessertsView from './menu/DessertsView';
import PastaView from './menu/PastaView';
import PzoneView from './menu/PzoneView';
import DealsView from './menu/DealsView';
import SidesView from './menu/SidesView';
import Header from '../components/Header';
import {
  pizzas,
  wings,
  desserts,
  drinks,
  sides,
  pzone,
  pasta,
  deals,
  tabsPizzas,
  tabsWings,
  tabsSides,
} from './../config/index';

const MenuView = (props) => {
  const { path } = useRouteMatch();

  const [activeTab, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  return (
    <Fragment>
      <Route path={path + '/pizzas'}>
        <Header title="PIZZAS" />
        <TabMenu
          activeTab={activeTab}
          handleChange={handleChange}
          menuOptions={tabsPizzas}
        />
        <PizzasView
          activeTab={activeTab}
          items={pizzas.popularPizzas}
          meats={pizzas.meats}
          chicken={pizzas.chicken}
          veggie={pizzas.veggie}
        />
      </Route>
      <Route path={path + '/wings'}>
        <Header title="WINGS" />
        <TabMenu
          activeTab={activeTab}
          handleChange={handleChange}
          menuOptions={tabsWings}
        />
        <WingsView
          activeTab={activeTab}
          items={wings.wingstreetWings}
          sides={wings.wingstreetSides}
          dips={wings.dips}
        />
      </Route>
      <Route path={path + '/drinks'}>
        <DrinksView items={drinks} />
      </Route>
      <Route path={path + '/desserts'}>
        <DessertsView items={desserts} />
      </Route>
      <Route path={path + '/pasta'}>
        <PastaView items={pasta} />
      </Route>
      <Route path={path + '/p-zone'}>
        <PzoneView items={pzone} />
      </Route>
      <Route path={path + '/deals'}>
        <DealsView items={deals} />
      </Route>
      <Route path={path + '/sides'}>
        <Header title="SIDES" />
        <TabMenu
          activeTab={activeTab}
          handleChange={handleChange}
          menuOptions={tabsSides}
        />
        <SidesView
          activeTab={activeTab}
          items={sides.sides}
          sauces={sides.sauces}
        />
      </Route>
    </Fragment>
  );
};

export default MenuView;
