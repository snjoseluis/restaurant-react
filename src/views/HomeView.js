import React, { Fragment } from 'react';
import Header from '../components/Header';
import ItemContainer from '../components/ItemContainer';

import { gql } from 'apollo-boost';

const deals = gql`
  {
    products(category: "deals") {
      id
      name
      description
      images {
        url
      }
    }
  }
`;

const HomeView = ({ title }) => {
  return (
    <Fragment>
      <Header title={title} />
      <ItemContainer query={deals} />
    </Fragment>
  );
};

export default HomeView;
