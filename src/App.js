import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom';
import { Container } from '@material-ui/core';
import { ApolloProvider } from '@apollo/react-hooks';

import './App.css';

import MenuView from './views/MenuView';
import CartView from './views/CartView';
import TopNavBar from './components/TopNavBar';
import { NavBar } from './components/navbar';
import Footer from './components/Footer';
import HomeView from './views/HomeView';

import { graphql } from './config/graphql';
import SessionContextProvider from './contexts/SessionContext';
import { ModalProvider } from './contexts/ModalContext/ModalContext';
import { ThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { AlertProvider } from './contexts/AlertContext/AlertContext';
import CartContextProvider from './contexts/CartContext';

const theme = createMuiTheme();

const App = () => {
  return (
    <BrowserRouter>
      <ThemeProvider theme={theme}>
        <AlertProvider>
          <ApolloProvider client={graphql}>
            <CartContextProvider>
              <SessionContextProvider>
                <ModalProvider>
                  <TopNavBar />
                  <NavBar />
                  <Container maxWidth="lg">
                    <Switch>
                      <Route exact path="/">
                        <HomeView title="Welcome to Restaurant APP" />
                      </Route>
                      <Route path="/menu" component={MenuView} />
                      <Route path="/cart" component={CartView} />
                    </Switch>
                    <Footer />
                  </Container>
                </ModalProvider>
              </SessionContextProvider>
            </CartContextProvider>
          </ApolloProvider>
        </AlertProvider>
      </ThemeProvider>
    </BrowserRouter>
  );
};

export default App;
